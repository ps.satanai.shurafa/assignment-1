import java.util.Scanner;

public class Matrix {
     int currentRow,rowNum,colNum;
     int[][] matrix,result;

    public Matrix(int[][] matrix){
        if (matrix == null) {
            throw new IllegalArgumentException("matrix is null");
        }
        this.matrix = matrix.clone();
        rowNum = matrix.length;
        colNum = matrix[0].length;
        currentRow = 0;
        result = new int[rowNum][colNum];
    }

    public void reinitialize(){
        colNum = matrix[0].length;
        rowNum = matrix.length;
        result = new int[rowNum][colNum];
        currentRow = 0;
    }

    public int getValue(int row, int col) {
        return matrix[row][col];
    }

    public boolean isLastCol(int i){
        return i == colNum - 1;
    }

    public boolean isLastRow(){
        return currentRow >= rowNum;
    }

    public boolean isSquare(int size){
        return isSquare() && rowNum == size;
    }

    private boolean isSquare() {
        return rowNum == colNum;
    }

    public boolean only2Rows(){
        return rowNum == 2;
    }

    public boolean only2Cols(){
        return colNum == 2;
    }

    public boolean isDiagonalPoint(int i, int currentRow){
        return i == currentRow;
    }

    public boolean isAboveDiagonal(int i, int currentRow){
        return i > currentRow;
    }

    public boolean isUnderDiagonal(int i, int currentRow){
        return i < currentRow;
    }

    public int[][] summation(int[][] matrix1){
        reinitialize();

        for(int i = 0; i< colNum; i++){
            result[currentRow][i] = matrix[currentRow][i]+matrix1[currentRow][i];

            if(isLastCol(i)){
                i = -1;
                currentRow++;
            }
            if(isLastRow()){
                break;
            }
        }
        return result;
    }

    public int[][] multiply(int num){
        reinitialize();

        for(int i = 0; i< colNum; i++) {
            result[currentRow][i] = matrix[currentRow][i] * num;

            if (isLastCol(i)) {
                i = -1;
                currentRow++;
            }
            if (isLastRow()) {
                break;
            }
        }
        return result;
    }

    public int[][] transpose(){
        reinitialize();
        result = new int[colNum][rowNum];

        for (int i = 0; i < rowNum; i++) {
            for (int j = 0; j < colNum; j++) {
                result[j][i] = matrix[i][j];
            }
        }
        return result;
    }

    public int[][] diagonal(){
        reinitialize();

        for(int i = 0; i< colNum; i++) {

            if(isDiagonalPoint(i,currentRow)){
                result[currentRow][i] = matrix[currentRow][i];
                continue;
            }
            else {
                result[currentRow][i] = 0;
            }

            if (isLastCol(i)) {
                i = -1;
                currentRow++;
            }
            if (isLastRow()) {
                break;
            }
        }
        return result;
    }

    public int[][] lowerTriangular(){
        reinitialize();

        for(int i = 0; i< colNum; i++) {

            if(isAboveDiagonal(i,currentRow)){
                result[currentRow][i] = 0;
            }
            else {
                result[currentRow][i] = matrix[currentRow][i];
            }

            if (isLastCol(i)) {
                i = -1;
                currentRow++;
            }
            if (isLastRow()) {
                break;
            }
        }
        return result;
    }

    public int[][] upperTriangular(){
        reinitialize();

        for(int i = 0; i< colNum; i++) {

            if(isUnderDiagonal(i,currentRow)){
                result[currentRow][i] = 0;
            }
            else {
                result[currentRow][i] = matrix[currentRow][i];
            }

            if (isLastCol(i)) {
                i = -1;
                currentRow++;
            }
            if (isLastRow()) {
                break;
            }
        }
        return result;
    }

    public int[][] subMatrix(){
        reinitialize();

        if(isSquare(2) || only2Rows()){
            colNum --;
        } else if (only2Cols()) {
            rowNum --;
        } else {
            colNum --;
            rowNum --;
        }
        result = new int[rowNum][colNum];

        for(int i = 0; i< colNum; i++) {
            result[currentRow][i] = matrix[currentRow][i];

            if (isLastCol(i)) {
                i = -1;
                currentRow++;
                System.out.println();
            }
            if (isLastRow()) {
                break;
            }
        }
        return result;
    }

    public int[][] subMatrix(int[][] matrix, int exRow, int exCol) {
        reinitialize();
        result = new int[colNum][rowNum];

        for (int row = 0; row < rowNum; row++) {
            if (row == exRow)
                continue;
            int newRow = row;
            if (row > newRow)
                newRow--;

            for (int col = 0; col < colNum; col++) {
                if (col == exCol)
                    continue;
                if (col > exCol)
                    result[newRow][col - 1] = matrix[row][col];
                else
                    result[newRow][col] = matrix[row][col];
            }
        }
        return result;
    }
    public boolean notEndOfTheCol(int currentElementIndex){
        return currentElementIndex != colNum-1;
    }

    public boolean leftColsEqualsRightRows(int rows){
        return colNum == rows;
    }

    public int[][] multiplication(int[][] matrix1){
        if(leftColsEqualsRightRows(matrix1.length)) {

            reinitialize();
            result = new int[rowNum][matrix1[0].length];
            int currentElementIndex = 0;

            for (int row = 0; row < rowNum; row++) {

                for (int col = 0; col < matrix1[0].length; col++) {
                    result[row][col] += matrix[row][currentElementIndex] * matrix1[currentElementIndex][col];

                    if (notEndOfTheCol(currentElementIndex)) {
                        col--;
                        currentElementIndex++;
                    } else {
                        currentElementIndex = 0;
                    }
                }

            }
        }
        return result;
    }

    public int determinant() {
        if (!isSquare())
            throw new ArithmeticException("NotSquareMatrix");
        int result = determinant(matrix, rowNum);
        return result;
    }

    private int determinant(int[][] matrix, int size) {
        if (size == 2)
            return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
        int result = 0;
        int sign = 1;
        for (int i = 0; i < size; i++) {
            int[][] subMatrix = subMatrix(matrix, i, 0);
            result += (sign *= -1) * matrix[0][i] * determinant(subMatrix, size - 1);
        }
        return result;
    }


    public static void main(String[] args) {
        int[][] matrix = new int[3][2];
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter a "+matrix.length+"*"+matrix[0].length+" matrix:");
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                matrix[i][j] = scan.nextInt();
            }
        }

        Matrix m = new Matrix(matrix);
        int[][] result = m.transpose();

        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                System.out.print(result[i][j]+ " ");
            }
            System.out.println();
        }
    }
}
